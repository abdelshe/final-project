package com.tunedapps.finalproject.dbs;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.tunedapps.finalproject.dao.NewsDao;
import com.tunedapps.finalproject.model.Data;

@Database(entities = {Data.class}, version = 1, exportSchema = false)
public abstract class NewsDBS extends RoomDatabase {

    public abstract NewsDao newsDao();

    private static NewsDBS ourInestance;

    public static NewsDBS getDBS(final Context context){
        if (ourInestance == null){
            ourInestance = Room.databaseBuilder(context,
                    NewsDBS.class, "NewsDBS").allowMainThreadQueries().build();
        }
        return ourInestance;
    }

}

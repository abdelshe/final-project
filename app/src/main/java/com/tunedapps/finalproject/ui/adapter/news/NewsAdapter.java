package com.tunedapps.finalproject.ui.adapter.news;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tunedapps.finalproject.R;
import com.tunedapps.finalproject.model.Data;
import com.tunedapps.finalproject.model.ModelNews;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    List<Data> newsModelArrayList;
    OnClickItem onClickItem;
    OnClickItemSendTittle  onClickItemSendTittle;

    public interface OnClickItem {
        public void newsItemClick(Data data,int  position);
    }

    public interface OnClickItemSendTittle {
        public void onClickItemSendTittle(String tittle);
    }


    public NewsAdapter(OnClickItem onClickItem,OnClickItemSendTittle onClickItemSendTittle,List<Data> newsModelArrayList) {
        this.newsModelArrayList = newsModelArrayList;
        this.onClickItem=onClickItem;
        this.onClickItemSendTittle=onClickItemSendTittle;
    }

    //to connect items in a row
    @NonNull
    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_news, parent, false);
        return new MyViewHolder(itemView);
    }

    //to connect data with the View item in a row
    @Override
    public void onBindViewHolder(@NonNull @NotNull NewsAdapter.MyViewHolder holder, int position) {
        if(newsModelArrayList != null) {
            holder.tvTitle.setText(newsModelArrayList.get(position).getTitle());
            holder.tvDesc.setText(newsModelArrayList.get(position).getDescription());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickItem.newsItemClick(newsModelArrayList.get(position),position);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickItemSendTittle.onClickItemSendTittle(newsModelArrayList.get(position).getTitle());
            }
        });
    }

    //list size
    @Override
    public int getItemCount() {
        if (newsModelArrayList != null)
            return newsModelArrayList.size();
        else
            return 0;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle;
        TextView tvDesc;
        ImageView imgNews;

        public MyViewHolder (@NonNull @NotNull View itemView){
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTittle);
            tvDesc = itemView.findViewById(R.id.tvDescription);
            imgNews= itemView.findViewById(R.id.imgNews);
        }
    }
}

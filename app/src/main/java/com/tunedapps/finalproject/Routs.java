package com.tunedapps.finalproject;

import com.tunedapps.finalproject.model.Data;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Routs {
    @GET("news?access_key=22d4b59bcaeb64b2028d6bd1c7716b1b")
    Call<List<Data>> getAllPublicNews();

}

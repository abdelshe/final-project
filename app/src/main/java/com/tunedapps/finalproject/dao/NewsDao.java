package com.tunedapps.finalproject.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.tunedapps.finalproject.model.Data;

import java.util.List;

@Dao
public interface NewsDao {
    @Insert( onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Data> data);

    @Query("select * from Data where language= :lang")
    Data getDataByType(String lang);
}

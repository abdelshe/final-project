package com.tunedapps.finalproject.base;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.tunedapps.finalproject.R;
import com.tunedapps.finalproject.ui.fragment.NewsFragment;

public class BaseActivity extends AppCompatActivity {

    FragmentManager fm;
    static Activity activity;
    void getView(){
        activity =BaseActivity.this;
        fm = ((BaseActivity) activity).getSupportFragmentManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        getView();

        openFragmentWithBundle(new NewsFragment(), "NewsFragment", null);
    }

    public void openFragmentWithBundle(Fragment fragment, String tag, Bundle bundle) {
        FragmentTransaction ft;
        fragment.setArguments(bundle);

        ft = fm.beginTransaction().replace(R.id.news_container, fragment, tag);
        ft.addToBackStack(tag);
        ft.commit();
    }
}
package com.tunedapps.finalproject.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.tunedapps.finalproject.R;


public class BaseFragment extends Fragment {

    FragmentManager fm;



    @Override
    public void onAttach(Activity activity) {


        super.onAttach(activity);
        fm = getActivity().getSupportFragmentManager();
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void openFragmentWithBundle(Fragment fragment, String tag, Bundle bundle) {
        FragmentTransaction ft;
        fragment.setArguments(bundle);
        //red
        ft = fm.beginTransaction().replace(R.id.news_container, fragment, tag);
        ft.addToBackStack(tag);
        ft.commit();
    }

    public void ShowProgress(View view) { view.setVisibility(View.VISIBLE); }
    public void hideProgress(View view) { view.setVisibility(View.GONE); }
}

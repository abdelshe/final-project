package com.tunedapps.finalproject.login.logwithemail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.tunedapps.finalproject.R;

import com.tunedapps.finalproject.base.BaseActivity;
import com.tunedapps.finalproject.login.register.RegisterActivity;
import com.tunedapps.finalproject.login.resetpassword.ResetPassword;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    EditText edt_email, edt_password;
    TextView txv_resetpassword;
    Button btn_login, btn_register;
    
    private FirebaseAuth mAuth;

    void getView(){
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        txv_resetpassword = findViewById(R.id.txv_resetpassword);
        btn_login = findViewById(R.id.btn_login);
        btn_register = findViewById(R.id.btn_createacc);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getView();
        
        mAuth = FirebaseAuth.getInstance();

        //setting an intent when login button is used
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String txtemail = edt_email.getText().toString().trim();
                String txtpassword = edt_password.getText().toString().trim();

                if (TextUtils.isEmpty(txtemail) || TextUtils.isEmpty(txtpassword)){
                    Toast.makeText(getApplicationContext(), "fill all the fields", Toast.LENGTH_SHORT).show();
                    return;
                }
                signIn(txtemail, txtpassword);
            }
        });

        //setting an intent when register button is used
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("Email",edt_email.getText().toString());
                startActivity(intent);
            }
        });

        //setting an intent when txv forgot password is used
        txv_resetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ResetPassword.class);
                intent.putExtra("Email",edt_email.getText().toString());
                startActivity(intent);
            }
        });
    }

        private void signIn(String email, String password) {

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                Log.d(TAG, "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                Intent intent = new Intent(LoginActivity.this, BaseActivity.class);
                                intent.putExtra("Email",edt_email.getText().toString());
                                startActivity(intent);

                            } else {

                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                Toast.makeText(LoginActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }
}
package com.tunedapps.finalproject.login.register;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.tunedapps.finalproject.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.tunedapps.finalproject.login.logwithemail.LoginActivity;

public class RegisterActivity extends AppCompatActivity {

    EditText edt_email, edt_password;
    Button btn_register;
    private FirebaseAuth mAuth;

    void getView() {
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        btn_register = findViewById(R.id.btn_register);
    }

    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getView();

        mAuth = FirebaseAuth.getInstance();

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpUser(edt_email.getText().toString(), edt_password.getText().toString());
            }
        });
    }
    private void signUpUser(String email, String password) {
        if (email.isEmpty()){
            Toast.makeText(this, "please fill email", Toast.LENGTH_SHORT).show();
        }

        if (password.isEmpty()){
            Toast.makeText(this, "please fill password", Toast.LENGTH_SHORT).show();
        }

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> Task) {
                        if (Task.isSuccessful()) {

                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.clear();
                            editor.putString("email", email);
                            editor.putString("password", password);
                            editor.apply();
                            Toast.makeText(RegisterActivity.this, getApplicationContext().getResources().getString(R.string.SuccessMessage),
                                    Toast.LENGTH_SHORT).show();

                            Log.d("result", "createUserWithEmail: Success");
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Log.w("TAG", "createUserWithEmail:failure", Task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
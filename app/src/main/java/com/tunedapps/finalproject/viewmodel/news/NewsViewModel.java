package com.tunedapps.finalproject.viewmodel.news;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.tunedapps.finalproject.model.Data;
import com.tunedapps.finalproject.model.ModelNews;
import com.tunedapps.finalproject.model.Root;
import com.tunedapps.finalproject.network.AppServices;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsViewModel extends AndroidViewModel {
    public NewsViewModel(@NonNull @NotNull Application application) {
        super(application);
    }


    List<Data> modelNewsArrayList = new ArrayList<>();


    public MutableLiveData<List<Data>> mutableLiveData = null;

    public MutableLiveData<List<Data>> getAllNews() {
        if (mutableLiveData == null) {
            mutableLiveData = new MutableLiveData<>();
            getNewsData();
        }
        return mutableLiveData;
    }


    public void getNewsData() {
        AppServices.routs().getAllPublicNews().enqueue(new Callback<List<Data>>() {
            @Override
            public void onResponse(Call<List<Data>> call, Response<List<Data>> response) {
                modelNewsArrayList=response.body();

                //send data to fragment
                mutableLiveData.setValue(modelNewsArrayList);
            }

            @Override
            public void onFailure(Call<List<Data>> call, Throwable t) {

            }
        });
    }
}
